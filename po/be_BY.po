# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# , 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-12 16:16+0000\n"
"PO-Revision-Date: 2016-03-18 20:25+0300\n"
"Last-Translator: \n"
"Language-Team: American English <kde-i18n-doc@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<"
"=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Language: be_BY\n"
"X-Source-Language: C\n"

#: ubuntu-mate-welcome:121
msgid "Fixing incomplete install succeeded"
msgstr "Выпраўленне незавершанага ўсталявання паспяхова скончана"

#: ubuntu-mate-welcome:122
msgid "Successfully fixed an incomplete install."
msgstr "Незавершанае ўсталяванне паспяхова выпраўлена."

#: ubuntu-mate-welcome:122
msgid "Fixing the incomplete install was successful."
msgstr "Выпраўленне незавершанага ўсталявання прайшло паспяхова."

#: ubuntu-mate-welcome:126
msgid "Fixing incomplete install failed"
msgstr "Выпраўленне незавершанага ўсталявання звалілася"

#: ubuntu-mate-welcome:127
msgid "Failed to fix incomplete install."
msgstr "Не выйшла выправіць незавершанае ўсталяванне."

#: ubuntu-mate-welcome:127
msgid "Fixing the incomplete install failed."
msgstr "Выпраўленне незавершанага ўсталявання скончылася з памылкай."

#: ubuntu-mate-welcome:134
msgid "Fixing broken dependencies succeeded"
msgstr "Выпраўленне сапсаваных залежнасцяў паспяхова скончана"

#: ubuntu-mate-welcome:135
msgid "Successfully fixed broken dependencies."
msgstr "Сапсаваныя залежнасці паспяхова выпраўлены."

#: ubuntu-mate-welcome:135
msgid "Fixing the broken dependencies was successful."
msgstr "Выпраўленне сапсаваных залежнасцяў прайшло паспяхова."

#: ubuntu-mate-welcome:139
msgid "Fixing broken dependencies failed"
msgstr "Выпраўленне сапсаваных залежнасцяў звалілася"

#: ubuntu-mate-welcome:140
msgid "Failed to fix broken dependencies."
msgstr "Не выйшла выправіць сапсаваныя залежнасці."

#: ubuntu-mate-welcome:140
msgid "Fixing the broken dependencies failed."
msgstr "Выпраўленне сапсаваных залежнасцяў скончылася з памылкай."

#: ubuntu-mate-welcome:193
msgid "Install"
msgstr "Усталяваць"

#: ubuntu-mate-welcome:194
msgid "Installation of "
msgstr "Усталяванне "

#: ubuntu-mate-welcome:195
msgid "installed."
msgstr "усталявана."

#: ubuntu-mate-welcome:197
msgid "Remove"
msgstr "Выдаліць"

#: ubuntu-mate-welcome:198
msgid "Removal of "
msgstr "Выдаленне "

#: ubuntu-mate-welcome:199
msgid "removed."
msgstr "выдалена."

#: ubuntu-mate-welcome:201
msgid "Upgrade"
msgstr "Абнавіць"

#: ubuntu-mate-welcome:202
msgid "Upgrade of "
msgstr "Абнаўленне "

#: ubuntu-mate-welcome:203
msgid "upgraded."
msgstr "абноўлена."

#: ubuntu-mate-welcome:208 ubuntu-mate-welcome:209
msgid "complete"
msgstr "завершана"

#: ubuntu-mate-welcome:209
msgid "has been successfully "
msgstr "было паспяхова "

#: ubuntu-mate-welcome:211 ubuntu-mate-welcome:212
msgid "cancelled"
msgstr "скасавана"

#: ubuntu-mate-welcome:212
msgid "was cancelled."
msgstr "было скасавана."

#: ubuntu-mate-welcome:214 ubuntu-mate-welcome:215
msgid "failed"
msgstr "завяршыўся памылкай"

#: ubuntu-mate-welcome:215
msgid "failed."
msgstr "завяршыўся памылкай."

#: ubuntu-mate-welcome:351
msgid "Blu-ray AACS database install succeeded"
msgstr "Усталяванне базы даных Blu-ray AACS паспяхова скончана"

#: ubuntu-mate-welcome:352
msgid "Successfully installed the Blu-ray AACS database."
msgstr "База даных Blu-ray AACS паспяхова ўсталявана"

#: ubuntu-mate-welcome:352
msgid "Installation of the Blu-ray AACS database was successful."
msgstr "Усталяванне базы даных Blu-ray AACS прайшло паспяхова"

#: ubuntu-mate-welcome:355
msgid "Blu-ray AACS database install failed"
msgstr "Усталяванне базы даных Blu-ray AACS звалілася"

#: ubuntu-mate-welcome:356
msgid "Failed to install the Blu-ray AACS database."
msgstr "Не выйшла ўсталяваць базу даных Blu-ray AACS."

#: ubuntu-mate-welcome:356
msgid "Installation of the Blu-ray AACS database failed."
msgstr "Усталяванне базы даных Blu-ray AACS скончылася з памылкай."

